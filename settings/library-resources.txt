*** Settings ***
Library           Selenium2Library
Library           String                                    #This is for string manipulation. No need to install
Library           DateTime                                  #This is for date and time value manipulation. No need to install
#Library           FakerLibrary                             #This is for test data generation. To install type, pip install robotframework-faker
#Library           HttpLibrary.HTTP                         #This is for API keywords. To install type, pip install robotframework-httplibrary