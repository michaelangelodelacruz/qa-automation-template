*** Variables ***


*** Keywords ***
Add Global Variables
	Generate Album Name

Generate Album Name
	${uniqueAlbumName}=		Random Album Name Generator		5
	Set Global Variable		${uniqueAlbumName}
	
Generate Helium Employee Details
	${random}=		Generate Random String
	
Add Employee 201 Global Variables Reaching The Max Limit
	${familyName}=						Generate Random String		21
	Set Global Variable					${familyName}
	${firstName}=						Generate Random String		51
	Set Global Variable					${firstName}
	Generate Random Email Address						31
	Set Global Variable					${email}
	${mobileNumber}=					Generate Random String		12		[NUMBERS]
	Set Global Variable					${mobileNumber}
	${homeNumber}=						Generate Random String		11		[NUMBERS]
	Set Global Variable					${homeNumber}
	${homeAddress}=						Generate Random String		201
	Set Global Variable					${homeAddress}
	${emergencyContactPerson}=			Generate Random String		121
	Set Global Variable					${emergencyContactPerson}
	${emergencyContactNumber}=			Generate Random String		12		[NUMBERS]
	Set Global Variable					${emergencyContactNumber}
	${emergencyContactRelationship}=	Generate Random String		12
	Set Global Variable					${emergencyContactRelationship}
	${jobName}=							Generate Random String		51
	Set Global Variable					${jobName}
	${employeeNumber}=					Generate Random String		6		[NUMBERS]
	Set Global Variable					${employeeNumber}		
	
Generate Random Family Name
	[Arguments]		${length}
	${familyName}=		Generate Random String		${length}
	Set Global Variable		${familyName}

Generate Random First Name
	[Arguments]		${length}
	${firstName}=		Generate Random String		${length}
	Set Global Variable		${firstName}
	
Generate Random Email Address
	[Arguments]		${length}
	${email}=		Generate Random String		${length}
	${email}=		Catenate		SEPARATOR=		${email}		@gmail.com
	Set Global Variable		${email}
	
Generate Random Number
	[Arguments]		${length}
	${number}=		Generate Random String		${length}		[NUMBERS]
	Set Global Variable		${number}
	
Generate Random Campaign Name
	[Arguments]		${length}
	${campaignName}=		Generate Random String		${length}
	Set Global Variable		${campaignName}
	
Generate Random Hashtag
	[Arguments]		${length}
	${hashtag}=		Generate Random String		${length}
	${hashtag}=		Catenate		SEPARATOR=		\#		${hashtag}
	Set Global Variable		${hashtag}

Generate Date Today                                                         #ja:added this keyword
    ${DateToday}=     Get Current Date
	${DateToday}=     Fetch From Left     ${DateToday}      ${SPACE}
	Set Global Variable		${DateToday}

Generate Random Name                                                        #ja:added this keyword
	[Arguments]		${length}
	${name}=		Generate Random String		${length}
	Set Global Variable		${name}

#Commit